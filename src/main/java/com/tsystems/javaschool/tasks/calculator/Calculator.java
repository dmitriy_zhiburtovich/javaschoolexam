package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;
import java.util.StringTokenizer;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private String trim(String statement) {

        if (statement != null) {

            statement = statement.replaceAll(" ", "");
            statement = statement.replaceAll("\\(\\+", "(0+");
            statement = statement.replaceAll("\\(\\-", "(0-");
            statement = statement.replaceAll("^\\+", "0+");
            statement = statement.replaceAll("^\\-", "0-");
            statement = statement.replaceAll(" ", "");
        }

        return statement;
    }

    private boolean isStatementCorrect(String str) {

        if (str == null) return false;

        if (!str.matches("^([\\-\\+\\*/\\)\\(\\d]|(\\d+\\.?\\d+))+$") ||
                str.matches("(^[\\)\\*/])+.*") ||
                str.matches(".*([\\*/\\(\\+\\-]$)+") ||
                str.matches(".*([\\+\\-\\*/][\\+\\-\\*/\\)$])+.*") ||
                str.matches(".*((\\d|(\\d+\\.?\\d+))[\\(])+.*") ||
                str.matches(".*(\\([\\*/\\)$])+.*") ||
                str.matches(".*(\\)([\\d\\(]|(\\d+\\.?\\d+)))+.*"))
            return false;

        int counter = 0;

        for (int i = 0; i < str.length() - 1; i++) {
            if (str.charAt(i) == '(') counter++;
            else if (str.charAt(i) == ')') counter--;
            if (counter < 0) return false;
            if (counter > 0 && i == str.length() - 1) return false;
        }

        return true;

    }

    private Integer getPriority(String operand){
        switch (operand){
            case "+":
            case "-": return 0;
            case "/":
            case "*": return 1;
        }
        return null;
    }

    private ArrayList<String> toReversePolishNotation(String str) {

        ArrayList<String> rpn = new ArrayList<>();
        Stack<String> stck = new Stack<>();

        StringTokenizer lexems = new StringTokenizer(str, "()+-*/", true);

        while (lexems.hasMoreTokens()) {
            String lexem = lexems.nextToken();

            switch (lexem) {
                case "+":
                case "-":
                case "*":
                case "/":

                    while(!stck.isEmpty() && !stck.peek().equals("(") &&
                            getPriority(lexem) <= getPriority(stck.peek())){
                        rpn.add(stck.pop());
                    }
                    stck.push(lexem);
                    break;

                case ")":

                    while(!stck.peek().equals("(")){
                        rpn.add(stck.pop());
                    }
                    stck.pop();
                    break;

                case "(": stck.push(lexem);
                    break;
                default: rpn.add(lexem);

            }
        }
        while(!stck.isEmpty()){
            rpn.add(stck.pop());
        }
        return rpn;
    }

    private BigDecimal compute(String operator, BigDecimal operand1, BigDecimal operand2){

        BigDecimal result = null;
        MathContext mc = new MathContext(20);
        switch (operator){
            case "+": result = operand2.add(operand1);
                break;
            case "-": result = operand2.subtract(operand1);
                break;
            case "*": result = operand2.multiply(operand1);
                break;
            case "/":
                result = operand2.divide(operand1, mc);
                break;
        }
        return result;
    }

    private BigDecimal calculate(ArrayList<String> rpn){
        Stack<BigDecimal> rpnStack = new Stack<>();
        Iterator it = rpn.iterator();
        while (it.hasNext()) {
            String current = (String) it.next();
            switch(current){
                case "+":
                case "-":
                case "/":
                case "*":
                    rpnStack.push(compute(current, rpnStack.pop(), rpnStack.pop()));
                    break;
                default: rpnStack.push(new BigDecimal(current));
            }
        }
        BigDecimal result = rpnStack.pop();
        result = result.setScale(4, BigDecimal.ROUND_HALF_UP);
        return result;
    }


    public String evaluate(String statement) {
        statement = trim(statement);

        if (isStatementCorrect(statement)) {
            ArrayList<String> reversePolishNotation = toReversePolishNotation(statement);
            try {
                String result = calculate(reversePolishNotation).toString();
                result = !result.contains(".") ? result
                        : result.replaceAll("0*$", "").replaceAll("\\.$", "");
                return result;
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return null;
            }

        }


        return null;
    }

}
