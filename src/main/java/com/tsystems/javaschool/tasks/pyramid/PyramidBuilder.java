package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int listSize = inputNumbers.size();
        int temp = 0;
        int numberofRows = 0;

        for (int i = 0; temp < listSize; i++) {
            temp += i;
            numberofRows = i;
        }

        if (inputNumbers.contains(null) || temp!= listSize || listSize >= Integer.MAX_VALUE - 1)
            throw new CannotBuildPyramidException();

        Collections.sort(inputNumbers);

        int[][] resultPyramide = new int[numberofRows][numberofRows * 2 - 1];

        int indent = numberofRows - 1;
        int counter = 0;

        for (int i = 0; i < resultPyramide.length; i++) {
            for (int j = indent; j < resultPyramide[i].length - indent; j++) {
                resultPyramide[i][j] = inputNumbers.get(counter);
                j++;
                counter++;
            }
            indent--;
        }

        return resultPyramide;
    }




}
